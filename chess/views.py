__author__ = 'kozhuhds'

from django.shortcuts import render
from chess.models import *
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from math import log, ceil
from forms import *
from django.db.models import Q

def all_players(request):
    player_list = Player.objects.all()
    active = True
    context = {
        'player_list': player_list,
        'pl_active': active
        }
    return render(request, 'all_participants.html', context)

def tournaments(request):
    tournaments_list = Tournament.objects.all()
    active = True
    context = {'tournaments_list': tournaments_list, 'tr_active': active}
    return render(request, 'tournaments.html', context)

def buhgoltz_koef(player, tournament):
    koef = 0
    player_parts = Part.objects.filter(Q(first_player = player) | Q(second_player = player))
    for part in player_parts:
        if part.first_player == player:
            koef += Scores.objects.get(player=part.second_player, tournament=tournament).value
        else:
            koef += Scores.objects.get(player=part.first_player, tournament=tournament).value
    return koef

def add_player(request):
    if request.method == 'POST':
        form = PlayerForm(request.POST)
        if form.is_valid():
            player = Player.objects.create(
                name=request.POST["name"],
                rating = request.POST["rating"],
                sex = request.POST["sex"],
            )
            player.save()
        else:
            tournaments_list = Tournament.objects.filter(step = 0)
            context = {
                'form': form,
                'tournaments_list': tournaments_list,
            }
            return render(request, 'add_player_error.html', context)
    else:
        form = PlayerForm()
    return HttpResponseRedirect(reverse('all_players'))


def add_tournament(request):
    if request.method == 'POST':
        form = TournamentForm(request.POST)
        if form.is_valid():
            tournament = Tournament.objects.create(
                name=request.POST["name"],
                description = request.POST["description"],
                date = request.POST["startdate"],
                )
            tournament.save()
        else:
            context = {
                'form': form
            }
            return render(request,"add_tournament_error.html", context)
    return HttpResponseRedirect(reverse('tournaments'))

def add_players_to_trt(request, tournament_id):
    curr_tournament = Tournament.objects.get(id = tournament_id)
    for key in request.POST:
        if request.POST[key] == 'on':
            player = Player.objects.get(id=key)
            player.tournament.add(tournament_id)
            player.save()
            pl_score = Scores.objects.create(
                player = player,
                tournament = curr_tournament
            )
            pl_score.save()
    return HttpResponseRedirect(reverse('curr_tournament', args=(tournament_id)))



def get_curr_tournament_results(request, tournament_id):
    curr_tournament = Tournament.objects.get(id = tournament_id)
    players_list = Player.objects.filter(tournament = curr_tournament)
    if players_list[0].b_koef == 0:
        for pl in players_list:
            pl.b_koef = buhgoltz_koef(pl, curr_tournament)
            pl.save()
    players_list = Player.objects.filter(player_scores__tournament = curr_tournament).order_by("-player_scores__value", "-b_koef")
    steps = []
    for st in range(1, curr_tournament.step):
        if st == tournament_id:
            steps.append((st, True))
        else:
            steps.append((st, False))
    players_list = list(players_list)
    for pl in players_list:
        pl.scores = Scores.objects.get(player=pl, tournament=curr_tournament).value
    context = {
        'tournament': curr_tournament,
        'player_list': players_list,
        'steps': steps
    }
    return render(request, 'final_results.html', context)

def get_curr_tournament_step(request, tournament_id, tournament_step):
    curr_tournament = Tournament.objects.get(id = tournament_id)
    parts = Part.objects.filter(tournament = curr_tournament, tournament_step = curr_tournament.step)
    players_list = list(Player.objects.filter(tournament = curr_tournament))
    for pl in players_list:
        pl.scores = Scores.objects.get(player=pl, tournament=curr_tournament).value
    tournament_is_end = ceil(log(len(players_list), 2) + log(2,2)) <= curr_tournament.step
    if int(curr_tournament.step) == 0 and not tournament_is_end:
        curr_tournament.step +=1
        curr_tournament.save()
    steps = []
    for st in range(1, curr_tournament.step+1):
        if st == int(tournament_step):
            steps.append((st, True))
        else:
            steps.append((st, False))
    if tournament_is_end:
        steps.pop()
    if len(parts) == 0 and not tournament_is_end:
        winner_group = list(Player.objects.filter(tournament = curr_tournament, prev_result = 1).order_by("rating"))
        lose_group = list(Player.objects.filter(tournament = curr_tournament, prev_result = -1).order_by("rating"))
        draw_group = list(Player.objects.filter(tournament = curr_tournament, prev_result = 0).order_by("rating"))
        if len(winner_group) % 2 == 1:
            lose_group.append(winner_group[len(winner_group) - 1])
            winner_group.pop()
        if len(lose_group) % 2 == 1:
            draw_group.append(lose_group[len(lose_group) - 1])
            lose_group.pop()

        if len(draw_group)%2 == 1:
            spare_player_scores = Scores.objects.get(player=draw_group[len(draw_group)-1], tournament=curr_tournament)
            spare_player_scores.value += 1
            spare_player_scores.save()
            draw_group[len(draw_group)-1].prev_result = 1
            draw_group[len(draw_group)-1].save()
        generate_parts(winner_group, parts, curr_tournament)
        generate_parts(lose_group, parts, curr_tournament)
        generate_parts(draw_group, parts, curr_tournament)

    curr_parts_list = Part.objects.filter(tournament = curr_tournament, tournament_step = tournament_step)
    context = {
        'tournament': curr_tournament,
        'player_list': players_list,
        'part_list': curr_parts_list,
        'steps': steps,
        'tournament_is_end': tournament_is_end
        }
    return render(request, 'curr_tournament_step.html', context)

def recalculate_elo(first_player, second_player, win):
    Ef = 1/1+(first_player.rating - second_player.rating)/40
    Es = 1/1+(second_player.rating - first_player.rating)/40
    k1 = 0
    k2=0
    if first_player.rating >= 2400:
        k1 = 10
    elif first_player.rating <2400:
        k1 = 15
    if second_player.rating >=2400:
        k2 = 10
    elif first_player.rating <2400:
        k2 = 15
    if int(win) == 1:
        first_player.rating = first_player.rating + k1 * (1 - Ef)
    elif int(win) == 2:
        second_player.rating = second_player.rating + k2 * (1 - Es)
    else:
        second_player.rating = second_player.rating + k2 * (0.5 - Es)
        first_player.rating = first_player.rating + k1 * (0.5 - Ef)


    first_player.save()
    second_player.save()

def generate_parts(list, parts, curr_tournament):
    i = 0
    while i < len(list)-1:
        new_part = parts.create(
            first_player = list[i],
            second_player = list[i+1],
            tournament_step = curr_tournament.step,
            tournament = curr_tournament
        )
        new_part.save()
        i += 2

def get_curr_tournament(request, tournament_id):
    curr_tournament = Tournament.objects.get(id = tournament_id)
    players_list = list(Player.objects.filter(tournament = curr_tournament).order_by("rating"))
    players_list_exclude = Player.objects.exclude(tournament = curr_tournament).order_by("rating")
    if len(players_list_exclude)==0:
        players_list_exclude = []
    for pl in players_list:
        pl.scores = Scores.objects.get(player=pl, tournament=curr_tournament).value
    steps = []
    for st in range(1, curr_tournament.step+1):
        steps.append(st)
    tournament_is_end = False
    if len(players_list) !=0:
        tournament_is_end = ceil(log(len(players_list), 2) + log(2,2)) <= curr_tournament.step
        if tournament_is_end:
            steps.pop()
    context = {'tournament': curr_tournament,
               'player_list': players_list,
               'steps': steps,
               'players_list_exclude': players_list_exclude,
               'tournament_is_end':tournament_is_end
    }
    return render(request, 'curr_tournament.html', context)



def change_result(request, part_id):
    curr_tournament = Tournament.objects.get(id = request.POST["tournament_id"])
    part = Part.objects.get(id = part_id)
    first_player = part.first_player
    second_player = part.second_player
    curr_result = int(request.POST["result"])
    if not part.is_served:
        if curr_result == 1:
            change_scores(first_player,curr_tournament,"plus",1)
        elif curr_result == 2:
            change_scores(second_player,curr_tournament,"plus",1)
        elif curr_result == 0:
            change_scores(first_player,curr_tournament,"plus",0.5)
            change_scores(second_player,curr_tournament,"plus",0.5)
        part.is_served = True
        recalculate_elo(first_player, second_player, curr_result)

    else:
        if int(part.result) == 1 and curr_result == 2:
            change_scores(second_player,curr_tournament,"plus",1)
            change_scores(first_player,curr_tournament,"minus",1)
        elif int(part.result) == 2 and curr_result == 1:
            change_scores(second_player,curr_tournament,"plus",1)
            change_scores(first_player,curr_tournament,"plus",1)
        elif int(part.result) == 2 and curr_result == 0:
            change_scores(second_player,curr_tournament,"minus",0.5)
            change_scores(first_player,curr_tournament,"plus",0.5)
        elif int(part.result) == 1 and curr_result == 0:
            change_scores(first_player,curr_tournament,"minus",0.5)
            change_scores(second_player,curr_tournament,"plus",0.5)
        elif int(part.result) == 0 and curr_result == 1:
            change_scores(first_player,curr_tournament,"plus",0.5)
            change_scores(second_player,curr_tournament,"minus",0.5)
        elif int(part.result) == 0 and curr_result == 2:
            change_scores(first_player,curr_tournament,"minus",0.5)
            change_scores(second_player,curr_tournament,"plus",0.5)
    part.result = request.POST["result"]
    part.save()
    if int(part.result) == 1:
        first_player.prev_result = 1
        second_player.prev_result = -1
    elif int(part.result) == 2:
        first_player.prev_result = -1
        second_player.prev_result = 1
    else:
        first_player.prev_result = 0
        second_player.prev_result = 0


    first_player.save()
    second_player.save()
    parts_list = Part.objects.filter(result = -1, tournament = request.POST["tournament_id"])
    if len(parts_list) == 0 and curr_tournament.step <= part.tournament_step:
        curr_tournament.step +=1
    curr_tournament.save()

    return HttpResponseRedirect(reverse('curr_tournament_step', args=(curr_tournament.id, part.tournament_step)))


def change_scores(player, tournament, operation, value):
    ps = Scores.objects.get(player=player, tournament=tournament)
    if operation == "plus":
        ps.value += value
    else:
        ps.value -= value
    ps.save()
__author__ = 'kozhuhds'

FEMALE = "FEMALE"
MALE = "MALE"
SEX_CHOICES = (
    (FEMALE, "Female"),
    (MALE, "Male")
)

FIRST_WIN = 1
SECOND_WIN = 2
DRAW = 0
NO_RESULT = -1
WIN_CHOICES = (
    (FIRST_WIN, "First win"),
    (SECOND_WIN, "Second win"),
    (DRAW, "Draw"),
    (NO_RESULT, "No result")
)
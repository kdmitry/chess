__author__ = 'kozhuhds'

from django.contrib import admin
from chess.models import *

class PlayerAdmin(admin.ModelAdmin):
    list_display = ['name', 'rating']

class PartAdmin(admin.ModelAdmin):
    list_display = ['first_player', 'second_player', 'result']


class TournamentAdmin(admin.ModelAdmin):
    list_display = ['name', 'description', 'date']

class ScoresAdmin(admin.ModelAdmin):
    list_display = ['player', 'tournament', 'value']

admin.site.register(Player, PlayerAdmin)
admin.site.register(Part, PartAdmin)
admin.site.register(Tournament, TournamentAdmin)
admin.site.register(Scores, ScoresAdmin)
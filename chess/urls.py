
from django.conf.urls import patterns, include, url
import settings

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
import views
admin.autodiscover()

urlpatterns = patterns('',

    # Examples:
    # url(r'^$', 'chess.views.home', name='home'),
    # url(r'^chess/', include('chess.foo.urls')),
    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    # Uncomment the next line to enable the admin:

    url(r'^$', views.tournaments, name='tournaments'),
    url(r'^players/$', views.all_players, name='all_players'),
    url(r'^tournaments/(?P<tournament_id>\d+)/?$', views.get_curr_tournament, name='curr_tournament'),
    url(r'^tournaments/(?P<tournament_id>\d+)/results$', views.get_curr_tournament_results, name='curr_tournament_results'),
    url(r'^tournaments/(?P<tournament_id>\d+)/step/(?P<tournament_step>\d+)$',
        views.get_curr_tournament_step, name='curr_tournament_step'),
    url(r'^add_participant/', views.add_player, name='add_player'),
    url(r'^tournaments/(?P<tournament_id>\d+)/add_players_to_trt/', views.add_players_to_trt, name='add_player_to_trt'),
    url(r'^change_result/(?P<part_id>\d+)', views.change_result, name='change_result'),
    url(r'^add_tournament/', views.add_tournament, name='add_tournament'),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
                           {'document_root': settings.MEDIA_ROOT}),
    url(r'^admin/', include(admin.site.urls))
)

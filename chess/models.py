from django.db import models
from constants import SEX_CHOICES, WIN_CHOICES

__author__ = 'kozhuhds'

class Tournament(models.Model):
    name = models.CharField(max_length=60)
    date = models.DateField()
    description = models.CharField(max_length=200, default="empty")
    step = models.IntegerField(default=0)
    def __str__(self):
        return self.name


class Player(models.Model):
    tournament = models.ManyToManyField(Tournament, default=None)
    name = models.CharField(max_length=50)
    rating = models.IntegerField(default=0)
    sex = models.CharField(choices=SEX_CHOICES, max_length=6, default="MALE")
    prev_result = models.IntegerField(default=0)
    b_koef = models.IntegerField(default=0)
    def __str__(self):
        return self.name

class Part(models.Model):
    first_player = models.ForeignKey(Player, related_name='first_player')
    second_player = models.ForeignKey(Player, related_name='second_player')
    tournament = models.ForeignKey(Tournament, related_name="part_of_tournament")
    result  = models.IntegerField(choices = WIN_CHOICES, default=-1)
    tournament_step = models.IntegerField(default=0)
    is_served = models.BooleanField(default=False)

class Scores(models.Model):
    player = models.ForeignKey(Player, related_name="player_scores")
    tournament = models.ForeignKey(Tournament)
    value = models.FloatField(default=0)

__author__ = 'kozhuhds'

from django import forms

class PlayerForm(forms.Form):
    name = forms.CharField(max_length=60, required=True)
    rating = forms.IntegerField(max_value=3000, required=True)
    trt = forms.HiddenInput()


class TournamentForm(forms.Form):
    name = forms.CharField(max_length=60, required=True)
    startdate = forms.DateField(required=True)
    description = forms.CharField(max_length=1000, required=True)